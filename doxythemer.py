#!/usr/bin/env python3

import sys, argparse, os, shutil, glob, http.server

from doxydocs.doxy_parser import DoxyParser

def prompt(question):
    print('{} (y/n)'.format(question), end=' ')
    return str(input()).strip().lower() == 'y'


def ensure_key(to_search, to_find):
    if to_search.get(to_find) is None: 
        print('Key "{}" missing from configuration. Unable to continue'.format(to_find))
        sys.exit(-1)


def do_serve_docs(config):
    outdir = config['OUTPUT_DIRECTORY']
    if not os.path.isdir(outdir):
        print('Error: Specified output directory {} is not a directory'.format(outdir))
        sys.exit(-1)

    
    print('Serving dcoumentation folder {} on 127.0.0.1:8080'.format(outdir))
    os.chdir(outdir)
    httpd = http.server.HTTPServer(('127.0.0.1', 8080), http.server.SimpleHTTPRequestHandler)
    httpd.serve_forever()


def do_build_docs(config):

    # Ensure that the specified input directory exists
    if not os.path.isdir(pargs.input_folder):
        print('"{}" is not a folder.  Cannot proceed ...'.format(pargs.input_folder))
        sys.exit(-1)

    # Make sure that the OUTPUT_DIRECTORY is a folder, empty, ...
    outdir = doxythemer_settings['OUTPUT_DIRECTORY'] 
    if os.path.exists(outdir) and not os.path.isdir(outdir):
        print('"{}" is not a folder.  Cannot proceed ...'.format(outdir))
        sys.exit(-1)

    outdir_contents = glob.glob(os.path.join(outdir, '*'))
    if len(outdir) > 0 and not pargs.assume_yes:
        if not prompt('Output directory {} is not empty. Proceed anyway ?'.format(outdir)):
            exit(-1)

    if os.path.exists(outdir):
        shutil.rmtree(outdir) 

    os.mkdir(outdir)

    # Configuration done, start working
    index = DoxyParser(pargs.input_folder)
    
    if index.load():
        index.render(outdir, doxythemer_settings)



if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Generate better docs from doxygen xml output')
    parser.add_argument(
        '-c', '--config', 
        type=str, 
        required=True, 
        help="The configuration file to use"
        )

    subparsers = parser.add_subparsers(dest='cmd')

    build_command = subparsers.add_parser('build', help="Generate the documentation")
    build_command.add_argument(
        'input_folder', 
        type=str, 
        default='.', 
        help='The folder containing the doxygen-generated xml'
        )
    build_command.add_argument(
        '-y', '--assume-yes', 
        action='store_true', 
        help='Answer all questions with "yes"'
        )

    serve_command = subparsers.add_parser('serve', help='Start an HTTP server on localhost:8080 to preview the generated documentation')
    # TODO: add IP/Port options

    pargs = parser.parse_args()
    print(vars(pargs))

    # Ensure that we can load the configuration file
    with open(pargs.config, 'r') as cfg_file:
        raw_config = compile(cfg_file.read(), pargs.config, 'exec')
        doxythemer_settings = {}
        exec(raw_config, doxythemer_settings)

    # Ensure that the OUTPUT_DIRECTORY key was set
    if doxythemer_settings.get('OUTPUT_DIRECTORY') is None:
        print('Required configuration value "OUTPUT_DIRECTORY" not found. Unable to continue')
        sys.exit(-1)

    if pargs.cmd == 'serve':
        do_serve_docs(doxythemer_settings)
    elif pargs.cmd == 'build':
        do_build_docs(doxythemer_settings)
