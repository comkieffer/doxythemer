
import os
from .doxy_element import DoxyElement

class DoxyExample(DoxyElement):

    def __init__(self, filename, xml_root, *args, **kwargs):
        super().__init__(filename, xml_root, path_prefix='example', *args, **kwargs)
        self._doxygen_example_name = self._read_compound_name()


    def render(self, doxygen_state, jinja_env, config):
        example_tpl = jinja_env.get_template('example.html')
        contents = example_tpl.render(
            example=self, **config
        )

        return (
            os.path.join(self.prefix, self.name + '.html'),
            contents
        )


    @property
    def name(self):
        return self._doxygen_example_name