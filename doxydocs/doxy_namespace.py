
import os
from collections import namedtuple
from lxml.etree import Element

from .doxy_errors import DoxyXMLError
from .doxy_element import DoxyElement

InnerClass = namedtuple('InnerClass', ['Protection', 'Name', 'id'])

class DoxyNamespace(DoxyElement):


    def __init__(self, filename: str, xml_root: Element, path_prefix: str='', *args, **kwargs):
        super().__init__(filename, xml_root, path_prefix=path_prefix, *args, **kwargs)
        self.name = self._read_compound_name()
        
        self.id = xml_root.attrib['id']
        if not self.id:
            raise DoxyXMLError('Unable to locate attribute "id" under <{} ...> node of {}'.format(
                xml_root.tag, filename
            ))

        self.language = xml_root.attrib['language']
        
        self.classes = [
            InnerClass(m.attrib['refid'], m.text, m.attrib['prot']) \
                for m in xml_root.findall('./innerclass')
        ]

        self.path = os.path.join(self.prefix, self.name + '.html')

    def render(self, doxygen_state, jinja_env, config):
        namespace_tpl = jinja_env.get_template('namespace.html')
        contents = namespace_tpl.render(
            namespace=self, **config
        )
        return (
            os.path.join(self.prefix, self.name + '.html'),
            contents
        )
