
import os, re

from .doxy_element import DoxyElement

class DoxyClass(DoxyElement):

    def __init__(self, filename, xml_root, *args, **kwargs):
        super().__init__(filename, xml_root, path_prefix='class', *args, **kwargs)
        self._doxygen_full_class_name = self._read_compound_name()

        # Extract namespaces and class name from original name
        matches = re.finditer(r":{0,2}(\w+)", self._doxygen_full_class_name)
        
        namespace_items = [match.group(1) for _, match in enumerate(matches)]
        self._doxygen_short_class_name = namespace_items[-1]
        self._doxygen_namespace_components = namespace_items[0:-1]

    def render(self, doxygen_state, jinja_env, config):
        class_tpl = jinja_env.get_template('class.html')
        contents = class_tpl.render(
            doxyclass=self, **config
        )

        return (
            os.path.join(self.prefix, *self.namespaces, self.name + '.html'),
            contents
        )

    @property
    def full_name(self):
        return self._doxygen_full_class_name


    @ property
    def name(self):
        return self._doxygen_short_class_name

    @property 
    def namespaces(self):
        return self._doxygen_namespace_components