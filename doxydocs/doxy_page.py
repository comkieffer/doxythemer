
import os
from lxml.etree import tostring

from .doxy_errors import DoxyXMLError
from .doxy_element import DoxyElement

class DoxyPage(DoxyElement):

    def __init__(self, filename, xml_root, *args, **kwargs):
        super().__init__(filename, xml_root, path_prefix='page', *args, **kwargs)
        
        self._doxygen_page_name = self._read_compound_name()
        self._doxygen_page_title = self._safe_find_node('./title').text
        self._doxygen_page_description = tostring(self._safe_find_node('./detaileddescription'), pretty_print=True).decode("utf-8") 

        # Make sure that the index page stays in the root directory
        if self.name == 'index':
            self._path_prefix = ''


    def render(self, doxygen_state, jinja_env, config):
        page_tpl = jinja_env.get_template('page.html')
        contents = page_tpl.render(
            page=self, **config
        )

        return (
            os.path.join(self.prefix, self.name + '.html'),
            contents
        )


    @property
    def name(self):
        return self._doxygen_page_name


    @property
    def title(self):
        return self._doxygen_page_title


    @property
    def description(self):
        return self._doxygen_page_description

    def __repr__(self):
        return '<doxydocs.doxy_page.DoxyPage title={}, desc={}...>'.format(
            self.title, self.description[1:50]
        )