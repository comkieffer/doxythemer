from lxml import etree

from .doxy_errors import DoxyXMLError

class DoxyElement:

    def __init__(self, filename, xml_root, path_prefix=''):
        self.filename = filename
        self._xml_root = xml_root

        # The path_prefix is a prefix that will be pre-pended to the destination path of the 
        # rendered template. This ensures that names are unique by putting namespaces under 
        # ``namespace/``, classes under ``class/``, ...
        self.prefix = path_prefix
        

    def _read_compound_name(self):
        return self._safe_find_node('./compoundname').text

    
    def _safe_find_node(self, xpath, root = None):
        if not root:
            root = self._xml_root

        found_node = root.find(xpath)
        if found_node is None:
            DoxyXMLError(
                'Unable to resolve xpath query "{}" on node {}'.format(
                    xpath, root.tag
                )
            )

        return found_node
    

    def render(self, doxyen_state, jinja_env, config):
        raise NotImplementedError()
    