
import os, glob, traceback, shutil

from lxml import etree
from lxml.etree import XMLSyntaxError

from jinja2 import Environment, FileSystemLoader

from doxydocs import *

# TODO: proper error handling
class DoxyParser: 
 
    def __init__(self, source_folder=None):
        self.source_folder = source_folder
        
        self.index = None
        
        self.dirs = {}
        self._default_dirs_prefix = 'dir'
        self.files = {} 
        self._default_file_prefix = 'file'
        self.pages = {}
        self._default_page_prefix = 'page'
        self.classes = {}
        self._default_class_prefix = 'class'
        self.namespaces = {}
        self._default_namespace_prefix = 'namespace'
        self.examples = {}
        self._default_example_prefix = 'example'
           

    def load(self):
        if not self.source_folder:
            raise DoxyError("Source folder not set, nothing to do")

        xml_files = glob.glob(os.path.join(self.source_folder, "*.xml"))
        if not xml_files:
            raise DoxyError("Unable to locate 'xml' files in source folder<{}>".format(self.source_folder))
        
        for xml_file in xml_files:
            try:
                self.parse_file(xml_file)
            except DoxyXMLError as ex:
                print('An error ocurred whilst parsing {}.\n'.format(xml_file))
                print(' => Original Error: {}\n'.format(ex))
                print('Traceback:\n')
                traceback.print_tb(ex.__traceback__)
                return False

        return True


    def parse_file(self, filename):
        with open(filename, 'r') as xml_file:
            try:
                xml_root = etree.parse(xml_file).getroot()
            except XMLSyntaxError as ex:
                raise DoxyError('Unable to parse file {}, Error = {}'.format(filename, ex))
        
        if xml_root.tag == 'doxygenindex':
            self.index = DoxyIndex(filename, xml_root)
            return

        compounddef_node = xml_root.find('./compounddef')
        if compounddef_node is None:
            raise DoxyXMLError('Unable to locate node <compounddef ...> under <{} ...>, the doxygen document is malformed'.format(xml_root.tag))

        if not compounddef_node.get('kind'):
            raise DoxyXMLError('<compounddef ...> node has no attribute "kind", unable to determin file type ...')                    
        
        kind = compounddef_node.attrib['kind']
        if kind == 'class' or kind == 'struct':
            doxy_class = DoxyClass(filename, compounddef_node)
            self.classes[doxy_class.name] = doxy_class
        elif kind == 'file':
            doxy_file = DoxyFile(filename, compounddef_node)
            self.files[doxy_file.name] = doxy_file
        elif kind == 'dir':
            doxy_dir = DoxyDir(filename, compounddef_node)
            self.dirs[doxy_dir.name] = doxy_dir
        elif kind == 'namespace':
            doxy_namespace = DoxyNamespace(
                filename, compounddef_node, path_prefix=self._default_namespace_prefix)
            self.namespaces[doxy_namespace.name] = doxy_namespace
        elif kind == 'example':
            doxy_example = DoxyExample(filename, compounddef_node)
            self.examples[doxy_example.name] = doxy_example
        elif kind == 'page':
            doxy_page = DoxyPage(filename, compounddef_node)
            self.pages[doxy_page.name] = doxy_page
        else:
            raise DoxyXMLError('Found unknown <kind> descriptor: {}'.format(kind))
    

    def render(self, output_dir, config_options):      
        module_root = os.path.dirname(os.path.abspath(__file__))
        default_template_path = os.path.join(module_root, 'templates/')
        static_files_dir = os.path.join(default_template_path, 'static/')
        
        shutil.copytree(static_files_dir, os.path.join(output_dir, 'static/'))

        jinja_env = Environment(
            loader=FileSystemLoader(default_template_path),
            trim_blocks=True,
            autoescape=True
        )

        all_doxygen_elements = {
            **self.dirs,
            **self.files,
            **self.pages,
            **self.classes, 
            **self.namespaces,
            **self.examples,
        }

        for _, element in all_doxygen_elements.items():
            desired_destination, contents = element.render(self, jinja_env, config_options)

            destination = os.path.join(output_dir, desired_destination)
            self._write_file(destination, contents)

        # TODO: generate index for classes, namespaces, examples, ...
        contents = jinja_env \
            .get_template('namespace_index.html') \
            .render(
                doxygen_state=self, config=config_options
            )
        
        destination = os.path.join(output_dir, self._default_namespace_prefix, 'index.html')
        self._write_file(destination, contents)

        
    def _write_file(self, filename, contents):
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, 'w+') as dest:
            dest.write(contents)
