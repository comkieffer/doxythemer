
class DoxyError(Exception):
    """ Base class for exception in the doxy_* classes """
    pass

class DoxyXMLError(DoxyError):
    """ Indicates that an error occurred whilst parsing an XML file """
    pass