
import os
from .doxy_element import DoxyElement

class DoxyDir(DoxyElement):

    def __init__(self, filename, xml_root, *args, **kwargs):
        super().__init__(filename, xml_root, path_prefix='dir', *args, **kwargs)
        self._doxygen_dirname = self._read_compound_name()
        

    def render(self, doxygen_state, jinja_env, config):
        dir_tpl = jinja_env.get_template('dir.html')
        contents = dir_tpl.render(
            dir=self, **config
        )

        return (
            os.path.join(self.prefix, self.name + '.html'),
            contents
        )

    @property
    def name(self):
        return self._doxygen_dirname