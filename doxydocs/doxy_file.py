
import os

from .doxy_element import DoxyElement

class DoxyFile(DoxyElement):

    def __init__(self, filename, xml_root, *args, **kwargs):
        super().__init__(filename, xml_root, path_prefix='file', *args, **kwargs)
        self._doxygen_filename = self._read_compound_name()


    def render(self, doxygen_state, jinja_env, config):
        file_tpl = jinja_env.get_template('file.html')
        contents = file_tpl.render(
            file=self, **config
        )

        return (
            os.path.join(self.prefix, self.name + '.html'),
            contents
        )


    @property
    def name(self):
        return self._doxygen_filename
