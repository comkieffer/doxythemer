

import os
from .doxy_element import DoxyElement

class DoxyIndex(DoxyElement):

    def __init__(self, filename, xml_root, *args, **kwargs):
        super().__init__(filename, xml_root, *args, **kwargs)

    def render(self, doxygen_state, jinja_env, config):
        pass
